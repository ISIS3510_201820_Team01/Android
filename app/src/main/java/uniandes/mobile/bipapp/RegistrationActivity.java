package uniandes.mobile.bipapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private static final String TAG = "RegistrationActivity";

    private EditText txtNewName;
    private EditText txtNewLastName;
    private EditText txtNewPhone;
    private EditText txtNewMail;
    private EditText txtNewPassword;
    private EditText txtNewPassword2;

    private Spinner spnNewGender;
    private EditText txtNewAge;

    private Button btnSendRegister;


    //Firebase Authentication
    private FirebaseAuth firebaseAuth;

    //Firestore
    private FirebaseFirestore database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);


        //Initialize firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        //Initialize database
        database = FirebaseFirestore.getInstance();

        //Reference view objects
        txtNewName = (EditText) findViewById(R.id.txtNewName);
        txtNewLastName = (EditText) findViewById(R.id.txtNewLastName);
        txtNewPhone = (EditText) findViewById(R.id.txtNewPhone);
        txtNewMail = (EditText) findViewById(R.id.txtNewMail);
        txtNewPassword = (EditText) findViewById(R.id.txtNewPassword);
        txtNewPassword2 = (EditText) findViewById(R.id.txtNewPassword2);

        txtNewAge = (EditText) findViewById(R.id.txtNewAge);

        //Spinner
        spnNewGender = (Spinner) findViewById(R.id.spnNewGender);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(RegistrationActivity.this,
                R.layout.spinner_item, getResources().getStringArray(R.array.genders));

        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnNewGender.setAdapter(myAdapter);


        btnSendRegister = (Button) findViewById(R.id.btnSendRegister);

        btnSendRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

    }

    private void registerUser(){
        final String newName = txtNewName.getText().toString().trim();
        final String newLastName = txtNewLastName.getText().toString().trim();
        final String newPhone = txtNewPhone.getText().toString().trim();
        final String newMail = txtNewMail.getText().toString().trim();
        String newPassword = txtNewPassword.getText().toString().trim();
        String newPassword2 = txtNewPassword2.getText().toString().trim();
        final String newAge = txtNewAge.getText().toString().trim();
        final String newGender = spnNewGender.getSelectedItem().toString().trim();


        if(TextUtils.isEmpty(newName) || TextUtils.isEmpty(newLastName) || TextUtils.isEmpty(newPhone) || TextUtils.isEmpty(newMail) || TextUtils.isEmpty(newPassword) || TextUtils.isEmpty(newPassword2) || TextUtils.isEmpty(newAge)){
            Toast.makeText(this, "Debes llenar todos los campos.", Toast.LENGTH_LONG).show();
            return;
        }

        if(!newPassword.equals(newPassword2)){
            Toast.makeText(this, "Las contraseñas ingresadas no coinciden.", Toast.LENGTH_LONG).show();
            return;
        }

        firebaseAuth.createUserWithEmailAndPassword(newMail, newPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()){
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            storeUser(newName, newLastName, newPhone, newMail, newAge, newGender, currentUser.getUid());
                            Toast.makeText(RegistrationActivity.this, "Se ha registrado el usuario correctamente.", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);

                        } else{
                            String errorMessage = task.getException().getMessage();

                            if(errorMessage.equals("The email address is badly formatted.")){
                                Toast.makeText(RegistrationActivity.this, "El correo ingresado no es válido.", Toast.LENGTH_LONG).show();
                            }
                            else if(errorMessage.equals("The email address is already in use by another account.")){
                                Toast.makeText(RegistrationActivity.this, "El correo ingresado ya fue tomado.", Toast.LENGTH_LONG).show();
                            }
                            else if(errorMessage.contains("The given password is invalid.")){
                                Toast.makeText(RegistrationActivity.this, "La contraseña debe tener mínimo 6 caracteres.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(RegistrationActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                            }
                        }

                    }
                });

    }


    private void storeUser(String newName, String newLastName, String newPhone, String newMail, String newAge, String newGender, final String userId){
        Map<String, Object> user = new HashMap<>();
        user.put("client", true);
        user.put("email", newMail);
        user.put("lastName", newLastName);
        user.put("name", newName);
        user.put("phone", newPhone);
        user.put("age", Integer.parseInt(newAge));

        if(newGender.equals("Masculino")){
            user.put("gender", "Male");
        } else{
            user.put("gender", "Female");
        }

        database.collection("Users").document(userId)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot written with ID: " + userId);

                        Toast.makeText(RegistrationActivity.this, "Registrado y guardado.", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                        //Toast.makeText(RegistrationActivity.this, "Error guardando el usuario.", Toast.LENGTH_SHORT).show();
                        Toast.makeText(RegistrationActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });



    }



}
