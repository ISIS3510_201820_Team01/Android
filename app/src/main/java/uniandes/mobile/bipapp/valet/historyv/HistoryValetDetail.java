package uniandes.mobile.bipapp.valet.historyv;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Date;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryValetDetail extends Fragment {


    private static final String TAG = "FirebaseLog";

    private FirebaseFirestore database;

    private View mView;


    private TextView fecha;

    private TextView hora;

    private TextView valet;

    private TextView costo;

    private TextView placa;

    private PickUpRequest currentRequest;

    private String docId;

    private PickUpRequest request;

    private User user;



    public HistoryValetDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_history_valet_detail, container, false);

        request = (PickUpRequest) getArguments().getSerializable(HistoryListValetAdapter.REQUEST_KEY);
        user = (User) getArguments().getSerializable(HistoryListValetAdapter.USER_KEY);

        fecha = (TextView) mView.findViewById(R.id.historyvalet_fecha);
        hora = (TextView) mView.findViewById(R.id.historyvalet_hora);
        valet = (TextView) mView.findViewById(R.id.historyvalet_nombre);
        costo = (TextView) mView.findViewById(R.id.historyvalet_tarifa);
        placa = (TextView) mView.findViewById(R.id.historyvalet_placa);

        Date d = new Date(request.getCreationTime().longValue());
        String format = new SimpleDateFormat("MMMM d,  yyyy").format(d);


        fecha.setText(format);


        String format2 = new SimpleDateFormat("h:mm a").format(d);

        hora.setText(format2);

        valet.setText(user.getName());

        costo.setText("$"+request.getEstimatedFare().intValue());

        placa.setText(request.getPlate());

        return mView;
    }

}
