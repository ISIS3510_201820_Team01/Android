package uniandes.mobile.bipapp.valet.currentv;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.TabsActivityValet;

public class GeneratorActivity extends AppCompatActivity {

    private Button generatorButton;
    private ImageView generatorImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generator);

        generatorButton = (Button) findViewById(R.id.btnGoBack);
        generatorImage = (ImageView) findViewById(R.id.generator_img);

        SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
        String idValet = preferences.getString("idValet", "idValet");

        SharedPreferences currentRequest = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        String idClient = currentRequest.getString("currentIdClient", "");
        Long confirmationTime = currentRequest.getLong("currentConfirmationTime", 0L);

        String confirmationTimeString = Long.toString(confirmationTime);


        if(!currentRequest.getBoolean("finished", true)){
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            try {
                //String qrMessage = idClient + idValet + confirmationTimeString;
                String qrMessage = idClient + idValet;
                BitMatrix bitMatrix = multiFormatWriter.encode(qrMessage, BarcodeFormat.QR_CODE, 230, 230);
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
                generatorButton.setText("Servicio Validado");
                generatorImage.setImageBitmap(bitmap);
                generatorImage.setVisibility(View.VISIBLE);

            } catch (WriterException e){
                e.printStackTrace();
            }
        } else{
            generatorButton.setText("Volver");
            generatorImage.setVisibility(View.INVISIBLE);

            generatorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openTabs(v);
                }
            });
            Toast.makeText(this, "No tiene ningún servicio confirmado", Toast.LENGTH_SHORT).show();
        }
        // cdyka0eQpAPPJUbkvOVqHwZtWb2IYWhGUEemUhot1538777671000.3738

//        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
//        try {
//            String qrMessage = idClient + idValet + confirmationTimeString;
//            BitMatrix bitMatrix = multiFormatWriter.encode(qrMessage, BarcodeFormat.QR_CODE, 230, 230);
//            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
//            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
//            generatorImage.setImageBitmap(bitmap);
//        } catch (WriterException e){
//            e.printStackTrace();
//        }


    }


    public void openTabs(View view) {

        SharedPreferences currentRequest = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = currentRequest.edit();

        editor.putString("currentIdClient", "");
        editor.putLong("currentConfirmationTime", 0L);
        editor.putBoolean("finished", true);

        editor.commit();

        Intent intent = new Intent(this, TabsActivityValet.class);
        startActivity(intent);

        finish();

    }


    public void openCarComments(View view) {
        SharedPreferences currentRequest = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = currentRequest.edit();

        editor.putString("currentIdClient", "");
        editor.putLong("currentConfirmationTime", 0L);
        editor.putBoolean("finished", true);

        editor.commit();

        Intent intent = new Intent(this, CarObservationActivity.class);
        startActivity(intent);

        finish();
    }
}