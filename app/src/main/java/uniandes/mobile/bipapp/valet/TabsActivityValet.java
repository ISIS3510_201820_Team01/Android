package uniandes.mobile.bipapp.valet;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import uniandes.mobile.bipapp.BipFragmentsInterface;
import uniandes.mobile.bipapp.MainActivity;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.client.TabsActivityClient;

public class TabsActivityValet extends AppCompatActivity {

    private Toolbar toolbar;
    public ViewPager pager;
    private ViewPagerAdapterValet adapter;
    public TabLayout tabLayout;
    private Button buttonLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_valet);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        pager = findViewById(R.id.pager);
        adapter = new ViewPagerAdapterValet(getSupportFragmentManager());
        pager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(pager);

        buttonLogOut = (Button) findViewById(R.id.button_logOut);

        pager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(final int position, final float v, final int i2) {
                    }

                    @Override
                    public void onPageSelected(final int position) {

                        if(position == 2)
                        {
                            BipFragmentsInterface fragment = (BipFragmentsInterface) adapter.instantiateItem(pager, position);
                            if (fragment != null) {
                                fragment.fragmentBecameVisible();
                            }

                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(final int position) {
                    }
                });


        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences currentPickup = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                String docId = currentPickup.getString("currentIdClient", "currentIdClient");

                SharedPreferences dropOff = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
                boolean dropoff = dropOff.getBoolean("DropOffValet", false);

                SharedPreferences credenciales = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
                credenciales.edit().clear().commit();

                SharedPreferences request = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                request.edit().clear().commit();

                SharedPreferences dropOffValet = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
                dropOffValet.edit().clear().commit();

                SharedPreferences dropOffClient = getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
                dropOffClient.edit().clear().commit();

                SharedPreferences currentDropOff = getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                currentDropOff.edit().clear().commit();

                SharedPreferences parquear = getSharedPreferences("Parquear", Context.MODE_PRIVATE);
                parquear.edit().clear().commit();

                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(TabsActivityValet.this, MainActivity.class);
                startActivity(intent);
                finish();

                return;


               /* if(!docId.equals("currentIdClient") || dropoff)
                {

                    Toast.makeText(getApplicationContext(),
                            "No es posible cerrar su sesión. Tiene servicios en curso.",
                            Toast.LENGTH_LONG).show();

                }
                else
                {

                }*/

            }
        });

        LinearLayout tabStrip = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return bloquarTabs();
                }
            });
        }
/*

        tabLayout.getChildAt(0).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return bloquarTabs();
                    }
                }
        );

        tabLayout.getChildAt(1).setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return bloquarTabs();
                    }
                }
        );
*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    private boolean bloquarTabs( )
    {
        SharedPreferences currentPickup = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        String docId = currentPickup.getString("currentIdClient", "currentIdClient");

        SharedPreferences dropOff = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
        boolean dropoff = dropOff.getBoolean("DropOffValet", false);


        if(!docId.equals("currentIdClient") || dropoff)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
