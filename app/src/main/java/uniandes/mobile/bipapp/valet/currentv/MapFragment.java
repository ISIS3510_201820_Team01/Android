package uniandes.mobile.bipapp.valet.currentv;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.api.Distribution;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;


import uniandes.mobile.bipapp.BipFragmentsInterface;
import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.TabsActivityValet;
import uniandes.mobile.bipapp.valet.currentv.GeneratorActivity;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;


public class MapFragment extends Fragment implements OnMapReadyCallback, BipFragmentsInterface {

    private static final String TAG = "MapsFragment";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final float DEFAULT_ZOOM = 15f;
    private static final int REQUEST_CODE = 1;

    private GoogleMap mMap;
    private MapView mMapView;
    private View mView;
    private Location mLocation;
    private LocationRequest mLocationRequest;

    private Marker pickUpMarker;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private PickUpRequest currentRequest;

    private Button mArrive;

    private FirebaseFirestore database;

    private Boolean parquearB;

    private Boolean dropoff;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_map, container, false);


        getCurrentRequest();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mArrive = (Button) mView.findViewById(R.id.btnArrive);

        SharedPreferences parquear = getActivity().getSharedPreferences("Parquear", Context.MODE_PRIVATE);
        parquearB = parquear.getBoolean("ParquearBoolean", false);

//        SharedPreferences dropOff = getActivity().getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
//        dropoff = dropOff.getBoolean("DropOffValet", false);

        if(parquearB)
        {
            mArrive.setText("CONFIRMACIÓN PARQUEO");
        }
        else
        {
            mArrive.setText("VALIDAR SERVICIO");
        }

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        if(currentRequest != null)
        {
            Log.d("LO LOGRE2:", currentRequest.getIdValet());


            SharedPreferences preferences = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            
            editor.putString("currentIdClient", currentRequest.getIdClient());
            editor.putString("currentRequestId", currentRequest.getId());
            editor.putLong("currentConfirmationTime", currentRequest.getConfirmationTime());
            editor.putBoolean("finished", false);

            editor.commit();

        }

        mArrive.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {

                SharedPreferences dropOff = getActivity().getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
                dropoff = dropOff.getBoolean("DropOffValet", false);

                if(!parquearB)
                {
                    if(!dropoff)
                    {
                        if(verifyConnectivity()) {
                            Intent intent = new Intent(getActivity(), GeneratorActivity.class);
                            startActivity(intent);
                        } else{
                            Toast.makeText(getActivity(), "Sin internet, no se puede realizar la acción", Toast.LENGTH_LONG).show();
                        }

                    }
                    else
                    {
                        dropOff.edit().clear().commit();
                        currentRequest = null;

                        Toast.makeText(getActivity(), "Entrega exitosa de vehículo", Toast.LENGTH_LONG).show();
                        Toast.makeText(getActivity(), "Gracias por trabajar con bip", Toast.LENGTH_LONG).show();

                        TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs);
                        tabhost.getTabAt(3).select();

                    }
                }
                else
                {
                    if(currentRequest != null)
                    {
                        database.collection("PickUpServices").document(currentRequest.getId())
                                .update(
                                        "parkLatitude", mLocation.getLatitude(),
                                        "parkLongitude", mLocation.getLongitude(),
                                        "carParked", true
                                );
                        currentRequest = null;


                    }

                    Toast.makeText(getActivity(), "Registro exitoso de parqueo", Toast.LENGTH_LONG).show();
                    Toast.makeText(getActivity(), "Gracias por trabajar con bip", Toast.LENGTH_LONG).show();

                    SharedPreferences parquear = getActivity().getSharedPreferences("Parquear", Context.MODE_PRIVATE);
                    parquear.edit().clear().commit();

                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs);
                    tabhost.getTabAt(3).select();

                }

            }
        });

        return mView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);

        }

    }

    private void getCurrentRequest( )
    {
        SharedPreferences request = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        String idClient = request.getString("currentIdClient", "currentIdClient");
        String reqId = request.getString("currentRequestId", "currentRequestId");
        String idValet = request.getString("currentIdValet", "currentIdValet");
        Long confirmationTime = request.getLong("currentConfirmationTime", 0L);
        Float latitudeClient = request.getFloat("latitudeClient", -2);
        Double latClient = latitudeClient.doubleValue();
        Float longitudeClient = request.getFloat("longitudeClient", -2);
        Double longClient = longitudeClient.doubleValue();

        if(idClient.equals("currentIdClient") ||
                reqId.equals("currentRequestId") ||
                idValet.equals("idValet") ||
                latitudeClient==-2 ||
                longitudeClient == -2
                )
        {
            currentRequest= null;
        }
        else
        {
            currentRequest = new PickUpRequest();
            currentRequest.setId(reqId);
            currentRequest.setIdValet(idValet);
            currentRequest.setIdClient(idClient);
            currentRequest.setConfirmationTime(confirmationTime);
            currentRequest.setLongitudeClient(longClient);
            currentRequest.setLatitudeClient(latClient);
        }


    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    getDeviceLocation();


                } else {
                    // permission denied, boo!

                    Toast.makeText(getContext(), "No se pudo acceder a su localizacion. Falta de permisos de su parte.", Toast.LENGTH_LONG).show();

                }
                return;
            }
        }
    }
*/

    //Gets the device location
    private void getDeviceLocation() {


        try {

            Task location = mFusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: found location!");
                        Location currentLocation = (Location) task.getResult();


                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);

                    } else {
                        Log.d(TAG, "onComplete: current location is null");
                        Toast.makeText(getActivity(), "Unable to get current location", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: Exception: " + e.getMessage());
        }
    }

    //Adjust camera in the map
    private void moveCamera(LatLng latLng, float zoom) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }

    LocationCallback mLocationCallback = new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for(Location location : locationResult.getLocations()){
                if(getActivity().getApplicationContext()!=null){

                    mLocation = location;

                    LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                    moveCamera(latLng, DEFAULT_ZOOM);

                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    //mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

                    if(currentRequest != null)
                    {
                        database.collection("PickUpServices").document(currentRequest.getId())
                                .update(
                                        "latitudeValet", location.getLatitude(),
                                        "longitudeValet", location.getLongitude()
                                );
                    }

                }
            }
        }
    };


    @Override
    public void onPause()
    {
        if(mMapView != null)
        {
            mMapView.onPause();

        }
        if(mFusedLocationProviderClient != null && mLocationCallback != null)
        {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }

        super.onPause();
    }

    @Override
    public void onStop()
    {
        if(mMapView != null)
        {
            mMapView.onStop();
        }

        if(mFusedLocationProviderClient != null && mLocationCallback != null)
        {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);

        }
        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(mMapView != null)
        {
            mMapView.onResume();
        }

        if(mMap != null)
        {
            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    if(mFusedLocationProviderClient != null & mLocationCallback != null)
                    {
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }

                }else{
                    checkLocationPermission();
                }
            }

        }


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000*5);
        mLocationRequest.setFastestInterval(3*1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if(currentRequest != null)
        {
            LatLng pickupLatLng = new LatLng(currentRequest.getLatitudeClient(), currentRequest.getLongitudeClient());
            mMap.addMarker(new MarkerOptions().position(pickupLatLng).title("Ubicación del cliente"));
        }


        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);

            }else{
                checkLocationPermission();
            }
        }


    }

    private void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Solicitud de permisos")
                        .setMessage("Por favor, proporcione los permisos necesarios")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            }
            else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case 1:{
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                       mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                       mMap.setMyLocationEnabled(true);
                    }
                } else{
                    Toast.makeText(getContext(), "Por favor, proporcione los permisos necesarios", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if(mMap != null)
            {
                if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        if(mFusedLocationProviderClient != null & mLocationCallback != null)
                        {
                            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                            mMap.setMyLocationEnabled(true);
                        }

                    }else{
                        checkLocationPermission();
                    }
                }
            }

        }
        else {
            if(mFusedLocationProviderClient != null && mLocationCallback != null)
            {
                mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
            }
        }
    }

    @Override
    public void fragmentBecameVisible() {
        SharedPreferences parquear = getActivity().getSharedPreferences("Parquear", Context.MODE_PRIVATE);
        parquearB = parquear.getBoolean("ParquearBoolean", false);

        getCurrentRequest();

//        SharedPreferences dropOff = getActivity().getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
//        dropoff = dropOff.getBoolean("DropOffValet", false);

        if(parquearB)
        {
            mArrive.setText("CONFIRMACIÓN PARQUEO");
        }
        else
        {
            mArrive.setText("VALIDAR SERVICIO");
        }
    }

  /*  @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        mMap = googleMap;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000*5);
        mLocationRequest.setFastestInterval(1000*3);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        int permissionCheck = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }
        else
        {
            getDeviceLocation();
        }



        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

    }*/


}
