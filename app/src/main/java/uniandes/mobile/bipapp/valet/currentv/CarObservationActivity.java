package uniandes.mobile.bipapp.valet.currentv;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.client.TabsActivityClient;
import uniandes.mobile.bipapp.valet.TabsActivityValet;

public class CarObservationActivity extends AppCompatActivity {

    private static final String TAG = "FirebaseLog";

    private ImageButton imgBtnLeftCar;
    private String leftComment;

    private ImageButton imgBtnRightCar;
    private String rightComment;

    private ImageButton imgBtnFrontCar;
    private String frontComment;

    private ImageButton imgBtnBackCar;
    private String backComment;

    private Button btnSendComments;

    private FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_observation);

        try {
            database = FirebaseFirestore.getInstance();
        } catch (Exception exc) {
            Log.d(TAG, exc.getMessage());
        }

        imgBtnLeftCar = (ImageButton) findViewById(R.id.imgBtnLeftCar);
        imgBtnRightCar = (ImageButton) findViewById(R.id.imgBtnRightCar);
        imgBtnFrontCar = (ImageButton) findViewById(R.id.imgBtnFrontCar);
        imgBtnBackCar = (ImageButton) findViewById(R.id.imgBtnBackCar);

        btnSendComments = (Button) findViewById(R.id.btnSendComments);

        leftComment = "";
        rightComment = "";
        backComment = "";
        frontComment = "";

        imgBtnLeftCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeLeftComment();
            }
        });

        imgBtnRightCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeRightComment();
            }
        });

        imgBtnFrontCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeFrontComment();
            }
        });

        imgBtnBackCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeBackComment();
            }
        });

        btnSendComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComments();
            }
        });


    }

    /**
     * Pop dialog to write left comment
     */
    private void writeLeftComment() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Parte izquierda");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(leftComment);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                leftComment = input.getText().toString().trim();
                Toast.makeText(CarObservationActivity.this, leftComment, Toast.LENGTH_SHORT).show();
                imgBtnLeftCar.setImageDrawable(getResources().getDrawable(R.drawable.izquierda_check));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    /**
     * Pop dialog to write right comment
     */
    private void writeRightComment() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Parte derecha");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(rightComment);

        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rightComment = input.getText().toString().trim();
                imgBtnRightCar.setImageDrawable(getResources().getDrawable(R.drawable.derecha_check));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /**
     * Pop dialog to write front comment
     */
    private void writeFrontComment() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Parte delantera");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(frontComment);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                frontComment = input.getText().toString().trim();
                imgBtnFrontCar.setImageDrawable(getResources().getDrawable(R.drawable.frente_check));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }


    /**
     * Pop dialog to write back comment
     */
    private void writeBackComment() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Parte trasera");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(backComment);

        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                backComment = input.getText().toString().trim();
                imgBtnBackCar.setImageDrawable(getResources().getDrawable(R.drawable.atras_check));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void sendComments() {

        if (TextUtils.isEmpty(frontComment) || TextUtils.isEmpty(rightComment) || TextUtils.isEmpty(leftComment) || TextUtils.isEmpty(backComment)) {
            Toast.makeText(this, "Se deben revisar todas las partes del vehículo", Toast.LENGTH_LONG).show();
            return;
        }

        //Mandar cosas a la colección de Firestore
        //String [] comments = new String[4];
        //comments[0] = frontComment;
        //comments[1] = rightComment;
        //comments[2] = backComment;
        //comments[3] = leftComment;

        List<String> comments = new ArrayList<String>();
        comments.add(frontComment);
        comments.add(rightComment);
        comments.add(backComment);
        comments.add(leftComment);


        SharedPreferences currentRequest = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        String idRequest = currentRequest.getString("currentRequestId", "");

        //Toast.makeText(this, "idRequest: " +  idRequest, Toast.LENGTH_SHORT).show();

        if (idRequest.length() > 0) {
            database.collection("PickUpServices").document(idRequest)
                    .update("comments", comments)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(CarObservationActivity.this, "Comentarios enviados exitosamente", Toast.LENGTH_LONG).show();
                            regresarACurrent();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(CarObservationActivity.this, "Error enviando los comentarios", Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    private void regresarACurrent()
    {
        SharedPreferences preferences = getSharedPreferences("Parquear", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean("ParquearBoolean", true);

        editor.commit();

        SharedPreferences request = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        request.edit().clear().commit();

        Intent intent = new Intent(this, TabsActivityValet.class);
        startActivity(intent);

        finish();

    }



}
