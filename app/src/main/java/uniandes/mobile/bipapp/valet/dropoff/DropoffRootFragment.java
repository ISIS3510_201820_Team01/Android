package uniandes.mobile.bipapp.valet.dropoff;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.dropoff.DropoffListFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class DropoffRootFragment extends Fragment {


    public DropoffRootFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_dropoff_root, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.dropoff_root, new DropoffListFragment());

        transaction.commit();

        return view;
    }

}
