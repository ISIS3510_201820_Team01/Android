package uniandes.mobile.bipapp.valet.pickup;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import uniandes.mobile.bipapp.valet.currentv.MapFragment;
import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickupconfirmFragment extends Fragment implements OnMapReadyCallback {

    private PickUpRequest request;

    private User user;

    private static final String TAG = "MapsFragment";

    private static final float DEFAULT_ZOOM = 15f;

    private GoogleMap mMap;
    private MapView mMapView;
    private View mView;

    private FirebaseFirestore database;

    private Button takeService;

    private TextView userName;

    private TextView userDistance;

    private TextView estimatedFare;


    public PickupconfirmFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_pickupconfirm, container, false);
        request = (PickUpRequest) getArguments().getSerializable(PickupListAdapter.REQUEST_KEY);
        user = (User) getArguments().getSerializable(PickupListAdapter.USER_KEY);


        Log.d("LO LOGRE:", request.getIdValet());

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
            return null;
        }

        takeService = (Button) mView.findViewById(R.id.takeService);

        userName = (TextView) mView.findViewById(R.id.userName);

        userDistance = (TextView) mView.findViewById(R.id.distanceToUser);

        estimatedFare = (TextView) mView.findViewById(R.id.estServiceFare);

        userName.setText(user.getName()+ " " + user.getLastName());
        userDistance.setText(String.format("%.2f", user.getDistance()) + " m"+"");
        estimatedFare.setText("$ " + request.getEstimatedFare().intValue());

        takeService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Modify the corresponding document with the confirmation.
                if(!verifyConnectivity())
                {
                    Toast.makeText(getContext(),
                            "No tiene conexion a internet. No podemos atender sus solicitudes",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    request.setConfirmed(true);
                    request.setConfirmationTime(System.currentTimeMillis());
                    database.collection("PickUpServices").document(request.getId())
                            .set(request)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully written!");
                                    Toast.makeText(getContext(),
                                            "Confirmacion exitosa. El usuario lo espera!",
                                            Toast.LENGTH_LONG).show();

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error writing document", e);
                                    Toast.makeText(getContext(),
                                            "Problemas con la confirmacion. Vuelva a intentar.",
                                            Toast.LENGTH_LONG).show();
                                }
                            });


                    SharedPreferences preferences2 = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor2 = preferences2.edit();

                    editor2.putString("currentIdClient", request.getIdClient());
                    editor2.putString("currentRequestId", request.getId());
                    editor2.putString("currentIdValet", request.getIdValet());
                    editor2.putLong("currentConfirmationTime", request.getConfirmationTime());
                    editor2.putFloat("latitudeClient", request.getLatitudeClient().floatValue());
                    editor2.putFloat("longitudeClient", request.getLongitudeClient().floatValue());
                    editor2.putBoolean("finished", false);

                    editor2.commit();

                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs);
                    tabhost.getTabAt(2).select();
                }


            }
        });
        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes",
                    Toast.LENGTH_LONG).show();
        }

        mMapView = (MapView) mView.findViewById(R.id.mapClientPosition);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);

        }

    }

    //Adjust camera in the map
    private void moveCamera(LatLng latLng, float zoom) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes",
                    Toast.LENGTH_LONG).show();
        }
        else
        {
            MapsInitializer.initialize(getContext());
            mMap = googleMap;

            LatLng client = new LatLng(request.getLatitudeClient(), request.getLongitudeClient());
            mMap.addMarker(new MarkerOptions().position(client).title("Posicion del usuario"));
            moveCamera(client, DEFAULT_ZOOM);

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);

        }

    }

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }

}
