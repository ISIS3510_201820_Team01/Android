package uniandes.mobile.bipapp.valet.pickup;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;
import uniandes.mobile.bipapp.client.TabsActivityClient;
import uniandes.mobile.bipapp.valet.TabsActivityValet;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickupListFragment extends Fragment {

    private static final String TAG = "FirebaseLog";
    private static final int REQUEST_CODE = 1;

    private RecyclerView pickuplist;
    private FirebaseFirestore database;

    private List<PickUpRequest> requestList;

    private List<User> usersList;

    private PickupListAdapter adapter;

    private Double latitudeValet;

    private Double longitudeValet;

    private String idValet;

    private ListenerRegistration listener;


    public PickupListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_pickup_list, container, false);


        requestList = new ArrayList<>();
        usersList = new ArrayList<>();
        adapter = new PickupListAdapter(requestList, usersList,this);

        pickuplist = (RecyclerView) view.findViewById(R.id.PickupList);
        pickuplist.setHasFixedSize(true);

        pickuplist.setAdapter(adapter);
        pickuplist.setLayoutManager(new LinearLayoutManager(getActivity()));


        SharedPreferences preferences = getContext().getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
        idValet = preferences.getString("idValet", "idValet");

        longitudeValet = 0.0;
        latitudeValet = 0.0;

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        int permissionCheck = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }
        else
        {
            getDeviceLocation();
        }

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. La lista mostrada no se actualizara en tiempo real.",
                    Toast.LENGTH_LONG).show();
        }



        if(listener == null)
        {

            getPickUpList();

        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if(listener != null)
        {

            listener.remove();
            listener = null;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(listener == null)
        {
            requestList = new ArrayList<>();
            usersList = new ArrayList<>();
            getPickUpList();

        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if(listener != null)
        {
            listener.remove();

        }
    }

    public void getPickUpList( )
    {
        listener=database.collection("PickUpServices").addSnapshotListener(
                new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "Error getting documents: " + e.getMessage());
                            return;
                        }

                        try
                        {
                            for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:

                                        Log.d(TAG, "New valet request: " + dc.getDocument().getData());
                                        PickUpRequest req = dc.getDocument().toObject(PickUpRequest.class);

                                        if(!req.getConfirmed())
                                        {
                                            req.setId(dc.getDocument().getId());
                                            req.setIdValet(idValet);
                                            req.setLatitudeValet(latitudeValet);
                                            req.setLongitudeValet(longitudeValet);

                                            User u = new User( );
                                            u.setId(req.getIdClient());
                                            getUserFromDB(u);
                                            u.setDistance(latitudeValet, longitudeValet, req.getLatitudeClient(), req.getLongitudeClient());
                                            req.setEstimatedFare(1500.0);

                                            requestList.add(req);
                                            usersList.add(u);

                                            Log.d(TAG, "Req id:" + req.getId());

                                            adapter.notifyDataSetChanged();
                                        }

                                        break;
                                    case MODIFIED:
                                        Log.d(TAG, "Modified request: " + dc.getDocument().getData());
                                        PickUpRequest modifiedReq = dc.getDocument().toObject(PickUpRequest.class);

                                        if(modifiedReq.getConfirmed())
                                        {
                                            removeRequestFromList(modifiedReq);
                                        }
                                        break;
                                    case REMOVED:
                                        Log.d(TAG, "Removed request: " + dc.getDocument().getData());
                                        PickUpRequest removedReq = dc.getDocument().toObject(PickUpRequest.class);
                                        removeRequestFromList(removedReq);
                                        break;
                                }
                            }


                        }
                        catch(Exception exp)
                        {
                            Log.d(TAG, exp.getMessage());

                        }


                    }
                });
        }

   private void getUserFromDB(final User u)
   {
       DocumentReference docRef = database.collection("Users").document(u.getId());
       docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
           @Override
           public void onComplete(@NonNull Task<DocumentSnapshot> task) {
               if (task.isSuccessful()) {
                   DocumentSnapshot document = task.getResult();
                   if (document.exists()) {
                       Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                       u.setEmail(document.getString("email"));
                       u.setName(document.getString("name"));
                       u.setLastName(document.getString("lastName"));
                       u.setPhone(document.getString("phone"));

                       adapter.notifyDataSetChanged();

                   } else {
                       Log.d(TAG, "No such client");
                   }
               } else {
                   Log.d(TAG, "get failed with ", task.getException());
               }
           }
       });

   }

    private void getDeviceLocation() {
        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try {

            Task location = mFusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: found location!");
                        Location currentLocation = (Location) task.getResult();

                        if(currentLocation != null)
                        {
                            latitudeValet = currentLocation.getLatitude();
                            longitudeValet = currentLocation.getLongitude();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "No es posible obtener tu ubicación actual. Verifica tu configuración de GPS y conexión a internet.", Toast.LENGTH_SHORT).show();
                        }

                        Log.d("LOCATION UPDATE: ", "Values Updated");

                        updateValetLocation();
                    } else {
                        Log.d(TAG, "onComplete: current location is null");
                        Toast.makeText(getActivity(), "No es posible obtener tu ubicación actual.", Toast.LENGTH_SHORT).show();
                    }

                }
            });


        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: Exception: " + e.getMessage());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    getDeviceLocation();


                } else {
                    // permission denied, boo!

                    Toast.makeText(getContext(), "No se pudo acceder a su localizacion. Falta de permisos de su parte.", Toast.LENGTH_LONG).show();

                }
                return;
            }
        }
    }
    public void updateValetLocation( )
    {
        Log.d("LOCATION UPDATE: ", "Values updated in list");
        int size = requestList.size();
        for(int i = 0; i < size; i++)
        {
            PickUpRequest req = requestList.get(i);
            requestList.get(i).setLongitudeValet(longitudeValet);
            requestList.get(i).setLatitudeValet(latitudeValet);

            User u = usersList.get(i);
            usersList.get(i).setDistance(latitudeValet,longitudeValet, req.getLatitudeClient(), req.getLongitudeClient());
            requestList.get(i).setEstimatedFare(usersList.get(i).getDistance()*30);
        }

        adapter.notifyDataSetChanged();
    }

    public void removeRequestFromList(PickUpRequest request)
    {
        String id = request.getId();
        int size = requestList.size();
        for(int i = 0; i < size; i++)
        {
            String idActual = requestList.get(i).getId();

            if(id.equals(idActual))
            {
                requestList.remove(i);
                usersList.remove(i);
            }
        }

        adapter.notifyDataSetChanged();

    }

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }
    }
