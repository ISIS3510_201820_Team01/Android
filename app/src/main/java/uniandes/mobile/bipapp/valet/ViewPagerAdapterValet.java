package uniandes.mobile.bipapp.valet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uniandes.mobile.bipapp.valet.currentv.MapFragment;
import uniandes.mobile.bipapp.TabsFragment;
import uniandes.mobile.bipapp.valet.dropoff.DropoffRootFragment;
import uniandes.mobile.bipapp.valet.historyv.HistoryValetRoot;
import uniandes.mobile.bipapp.valet.pickup.PickupRootFragment;


public class ViewPagerAdapterValet extends FragmentPagerAdapter
{
    public ViewPagerAdapterValet(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i)
    {
        Fragment item = null;
        switch(i)
        {
            case 0:
                item = new PickupRootFragment();
                break;
            case 1:
                item = new DropoffRootFragment();
                break;
            case 2:
                 item = new MapFragment();
                 break;
            case 3:
                 item = new HistoryValetRoot();
                 break;
        }

        Bundle bundle = new Bundle();
        item.setArguments(bundle);


        return item;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";
        switch(position)
        {
            case 0: title="Recoger"; break;
            case 1: title = "Entregar"; break;
            case 2: title = "Actual"; break;
            case 3: title = "Historial"; break;
        }
        return title;
    }
}
