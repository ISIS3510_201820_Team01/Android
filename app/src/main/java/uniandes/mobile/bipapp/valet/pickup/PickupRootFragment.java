package uniandes.mobile.bipapp.valet.pickup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.pickup.PickupListFragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class PickupRootFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        /* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.fragment_pickup_root, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.pickup_root, new PickupListFragment());

        transaction.commit();

        return view;
    }



}
