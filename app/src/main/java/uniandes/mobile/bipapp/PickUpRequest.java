package uniandes.mobile.bipapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PickUpRequest implements Serializable
{
    private String id;

    private Long confirmationTime;
    private Boolean confirmed;

    private Double creationTime;

    private Double estimatedFare;

    private String idClient;
    private Double latitudeClient;
    private Double longitudeClient;

    private String idValet;
    private Double latitudeValet;
    private Double longitudeValet;

    private String plate;

    private String qr;

    private Boolean approved;

    private ArrayList<String> comments;

    private Boolean carParked;

    private Boolean validated;

    private Double parkLatitude;

    private Double parkLongitude;

    public PickUpRequest() {
    }

    public PickUpRequest(Boolean confirmed, Double creationTime, String idClient,
                         Double latitudeClient, Double longitudeClient, String plate) {
        this.confirmed = confirmed;
        this.creationTime = creationTime;
        this.idClient = idClient;
        this.latitudeClient = latitudeClient;
        this.longitudeClient = longitudeClient;
        this.plate = plate;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Double getParkLatitude() {
        return parkLatitude;
    }

    public void setParkLatitude(Double parkLatitude) {
        this.parkLatitude = parkLatitude;
    }

    public Double getParkLongitude() {
        return parkLongitude;
    }

    public void setParkLongitude(Double parkLongitude) {
        this.parkLongitude = parkLongitude;
    }

    public Boolean getCarParked() {
        return carParked;
    }

    public void setCarParked(Boolean carParked) {
        this.carParked = carParked;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(ArrayList<String> comments) {
        this.comments = comments;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getConfirmationTime() {
        return confirmationTime;
    }

    public void setConfirmationTime(Long confirmationTime) {
        this.confirmationTime = confirmationTime;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Double getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Double creationTime) {
        this.creationTime = creationTime;
    }

    public Double getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Double estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public Double getLatitudeClient() {
        return latitudeClient;
    }

    public void setLatitudeClient(Double latitudeClient) {
        this.latitudeClient = latitudeClient;
    }

    public Double getLongitudeClient() {
        return longitudeClient;
    }

    public void setLongitudeClient(Double longitudeClient) {
        this.longitudeClient = longitudeClient;
    }

    public String getIdValet() {
        return idValet;
    }

    public void setIdValet(String idValet) {
        this.idValet = idValet;
    }

    public Double getLatitudeValet() {
        return latitudeValet;
    }

    public void setLatitudeValet(Double latitudeValet) {
        this.latitudeValet = latitudeValet;
    }

    public Double getLongitudeValet() {
        return longitudeValet;
    }

    public void setLongitudeValet(Double longitudeValet) {
        this.longitudeValet = longitudeValet;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }
}
