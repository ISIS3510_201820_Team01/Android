package uniandes.mobile.bipapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.newrelic.agent.android.NewRelic;

import uniandes.mobile.bipapp.client.TabsActivityClient;
import uniandes.mobile.bipapp.valet.TabsActivityValet;

import static com.newrelic.agent.android.measurement.MeasurementType.Activity;

public class MainActivity extends AppCompatActivity {

    //True:client and false:valet
    private boolean appMode;

    private Button buttonRegister;
    private Button buttonSignIn;

    private EditText txtMail;
    private EditText txtPassword;

    //Authentication
    private FirebaseAuth firebaseAuth;

    //Firestore
    private FirebaseFirestore database;

    private GoogleApiClient googleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appMode = true;

        setContentView(R.layout.activity_main);

        //Initialize firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        //Initialize database
        database = FirebaseFirestore.getInstance();


        buttonRegister = findViewById(R.id.button_register);
        buttonSignIn = findViewById(R.id.button_signin);
        txtMail = (EditText) findViewById(R.id.txtMail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        //SignUp user with button
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp();
            }
        });
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn();
            }
        });



       /* NewRelic.withApplicationToken(
                "AA09459fbfd32bc33758171d82caad6565c7385817"
        ).start(this.getApplication());*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Check if user is signed in
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if(currentUser != null){
            //Toast.makeText(this, "Ya estás logeado como: " + currentUser.getEmail(), Toast.LENGTH_SHORT).show();
            checkUserRole(currentUser.getUid());

        } else{
            SharedPreferences credenciales = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
            credenciales.edit().clear().commit();

            SharedPreferences request = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
            request.edit().clear().commit();

            SharedPreferences dropOffValet = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
            dropOffValet.edit().clear().commit();

            SharedPreferences dropOffClient = getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
            dropOffClient.edit().clear().commit();

            SharedPreferences currentDropOff = getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
            currentDropOff.edit().clear().commit();

            SharedPreferences parquear = getSharedPreferences("Parquear", Context.MODE_PRIVATE);
            parquear.edit().clear().commit();
            //Toast.makeText(this, "Falta iniciar sesión!", Toast.LENGTH_SHORT).show();
        }

    }

    public void openTabs(View view) {

        SharedPreferences credenciales = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
        credenciales.edit().clear().commit();

        SharedPreferences request = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        request.edit().clear().commit();

        SharedPreferences dropOffValet = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
        dropOffValet.edit().clear().commit();

        SharedPreferences dropOffClient = getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
        dropOffClient.edit().clear().commit();

        if(appMode)
        {
            SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);

            String idClient = "cdyka0eQpAPPJUbkvOVq";

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("idClient", idClient);

            editor.commit();

            Intent intent = new Intent(this, TabsActivityClient.class);
            startActivity(intent);

        }
        else
        {
            SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);

            String idValet = "HwZtWb2IYWhGUEemUhot";

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("idValet", idValet);

            editor.commit();

            Intent intent = new Intent(this, TabsActivityValet.class);
            startActivity(intent);
        }

    }


    /**
     * Pass to RegistrationActivity to SignUp new user.
     */
    public void signUp(){
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    /**
     * Sign in existing user.
     */
    public void signIn(){
        String mail = txtMail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        if(TextUtils.isEmpty(mail) || TextUtils.isEmpty(password)){
            Toast.makeText(this, "Debes ingresar correo y contraseña para iniciar sesión.", Toast.LENGTH_LONG).show();
            return;
        }

        firebaseAuth.signInWithEmailAndPassword(mail, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(MainActivity.this, "Inició sesión correctamente!", Toast.LENGTH_LONG).show();
                            FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                            if(currentUser != null){
                                checkUserRole(currentUser.getUid());
                            }
                        }
                        else {

                            String errorMessage = task.getException().getMessage();
                            //Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();

                            if(errorMessage.equals("The email address is badly formatted.")){
                                Toast.makeText(MainActivity.this, "El correo ingresado no es válido.", Toast.LENGTH_LONG).show();
                            }
                            else {
                                Toast.makeText(MainActivity.this, "Correo o contraseña incorrectos.", Toast.LENGTH_LONG).show();
                            }
                            //Toast.makeText(MainActivity.this, "Error iniciando sesión.", Toast.LENGTH_LONG).show();

                        }
                    }
                });





    }

    /**
     * When logged in, checks the user's role to determine which tabs to open.
     * @param userId
     */
    public void checkUserRole(String userId){
        DocumentReference docRef = database.collection("Users").document(userId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot document = task.getResult();
                    boolean isClient = document.getBoolean("client");
                    openTabs2(isClient);

                } else{
                    Toast.makeText(MainActivity.this, "Error consultando el rol del usuario.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    /**
     * New OpenTabs checking the user's role from Firestore
     * @param userRole
     */
    public void openTabs2(boolean userRole){



        if(userRole){
            SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);

            String idClient = firebaseAuth.getCurrentUser().getUid();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("idClient", idClient);

            editor.commit();

            Intent intent = new Intent(this, TabsActivityClient.class);
            startActivity(intent);
            finish();

        } else{
            SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);

            String idValet = firebaseAuth.getCurrentUser().getUid();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("idValet", idValet);

            editor.commit();

            Intent intent = new Intent(this, TabsActivityValet.class);
            startActivity(intent);
            finish();
        }

    }

}
