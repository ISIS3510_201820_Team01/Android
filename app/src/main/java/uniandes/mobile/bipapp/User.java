package uniandes.mobile.bipapp;

import java.io.Serializable;

public class User implements Serializable
{
    private String id;

    private String name;

    private String lastName;

    private String email;

    private String phone;


    //Distancia del valet al usuario en caso de requerirse.
    private Double distance;

    public User() {
    }

    public User(String id, String name, String lastName, String email, String phone) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.distance = -1.0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double latitudeValet, Double longitudeValet, Double latitudeUser, Double longitudeUser)
    {
        double radlatValet = Math.PI * (latitudeValet/180);

        double radlatUser = Math.PI * (latitudeUser/180);

        double radlonValet = Math.PI * (longitudeValet/180);

        double radlonUser = Math.PI * (longitudeUser/180);

        double theta = longitudeValet-longitudeUser;

        double radtheta = Math.PI * (theta/180);

        double dist = Math.sin(latitudeUser) * Math.sin(latitudeValet) + Math.cos(latitudeUser) * Math.cos(latitudeValet) * Math.cos(radtheta);

        dist = Math.acos(dist);

        dist = dist * 180/Math.PI;

        dist = dist * 60 * 1.1515;

        dist = dist * 1.609344;

        this.distance = dist;
    }
}
