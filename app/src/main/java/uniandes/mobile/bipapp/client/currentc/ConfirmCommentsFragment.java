package uniandes.mobile.bipapp.client.currentc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.List;

import javax.annotation.Nullable;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;

public class ConfirmCommentsFragment extends Fragment {

    private static final String TAG = "FirebaseLog";

    private FirebaseFirestore database;

    private View mView;

    private String docId;

    private Button btnConfirmComments;

    private String valetRating;


    private TextView txtFrontComment;
    private TextView txtRightComment;
    private TextView txtBackComment;
    private TextView txtLeftComment;

    private ListenerRegistration snapShot;


    public ConfirmCommentsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_confirm_comments, container, false);

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        SharedPreferences currentRequest = getActivity().getSharedPreferences("PruebaCurrentPickUp", Context.MODE_PRIVATE);
        docId = currentRequest.getString("docId", "currentPickUpId");

        btnConfirmComments = (Button) mView.findViewById(R.id.btnConfirmComments);

        txtFrontComment = (TextView) mView.findViewById(R.id.txtFrontComment);
        txtRightComment = (TextView) mView.findViewById(R.id.txtRightComment);
        txtBackComment = (TextView) mView.findViewById(R.id.txtBackComment);
        txtLeftComment = (TextView) mView.findViewById(R.id.txtLeftComment);

        txtFrontComment.setVisibility(View.INVISIBLE);
        txtRightComment.setVisibility(View.INVISIBLE);
        txtBackComment.setVisibility(View.INVISIBLE);
        txtLeftComment.setVisibility(View.INVISIBLE);




        if(!docId.equals("currentPickUpId")){
            getCurrentComments();
        }

        btnConfirmComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                database.collection("PickUpServices").document(docId)
                        .update(
                                "approved", true
                        );

                followCar();

            }
        });




        return mView;
    }

    @Override
    public void onStop( ) {

        super.onStop();

        if(snapShot != null)
        {
            snapShot.remove();
        }

    }


    public void getCurrentComments(){

        Log.d("WAITING FRAG", docId);



        snapShot = database.collection("PickUpServices").document(docId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                Log.d("WAITING FRAG", "Cambio algo ");


                if(e != null){
                    Toast.makeText(getActivity(), "Error Cargando ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.toString());
                    return;
                }

                if(documentSnapshot != null && documentSnapshot.exists()){

                    List<String> comments = (List<String>) documentSnapshot.get("comments");


                    if(comments != null && comments.size() > 0){

                        txtFrontComment.setText(comments.get(0));
                        txtRightComment.setText(comments.get(1));
                        txtBackComment.setText(comments.get(2));
                        txtLeftComment.setText(comments.get(3));

                        txtFrontComment.setVisibility(View.VISIBLE);
                        txtRightComment.setVisibility(View.VISIBLE);
                        txtBackComment.setVisibility(View.VISIBLE);
                        txtLeftComment.setVisibility(View.VISIBLE);
                    }


                }
            }
        });
    }

    public void followCar(){

        Toast.makeText(getActivity(), "Aceptados los comentarios", Toast.LENGTH_SHORT).show();

        SharedPreferences preferences = getActivity().getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putBoolean("DropOff", true);

        editor.commit();

        SharedPreferences request = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        request.edit().clear().commit();



        //rateValet();

        getActivity().finish();
        getActivity().overridePendingTransition(0, 0);
        startActivity(getActivity().getIntent());
        getActivity().overridePendingTransition(0, 0);

    }


    public void rateValet(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Califica el servicio");

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setKeyListener(DigitsKeyListener.getInstance("012345"));
        input.setSingleLine(true);
        input.setText(valetRating);

        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                valetRating = input.getText().toString().trim();

                saveRating();

            }
        });


        builder.show();
    }

    public void saveRating(){
        database.collection("PickUpServices").document(docId)
                .update(
                        "valetRating", Integer.parseInt(valetRating)
                );

        Toast.makeText(getActivity(), "Su calificación de " + valetRating + " fue enviada exitosamente.", Toast.LENGTH_LONG).show();

        getActivity().finish();
        getActivity().overridePendingTransition(0, 0);
        startActivity(getActivity().getIntent());
        getActivity().overridePendingTransition(0, 0);
    }


}
