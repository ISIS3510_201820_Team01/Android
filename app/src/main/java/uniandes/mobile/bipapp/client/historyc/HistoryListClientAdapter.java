package uniandes.mobile.bipapp.client.historyc;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryListClientAdapter extends RecyclerView.Adapter<HistoryListClientAdapter.ViewHolder> {

    public List<PickUpRequest> requestList;

    public List<User> usersList;

    public HistoryListClientFragment listFragment;

    public static final String REQUEST_KEY = "request_key";

    public static final String USER_KEY = "user_key";


    public HistoryListClientAdapter(List<PickUpRequest> requestList, List<User> userList, HistoryListClientFragment fragment)
    {
        this.requestList = requestList;
        this.usersList = userList;
        listFragment = fragment;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_list_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i)
    {
        final PickUpRequest actual = requestList.get(i);
        final User uActual = usersList.get(i);

        Date d = new Date(actual.getCreationTime().longValue());
        String format = new SimpleDateFormat("MMM d,  yyyy h:mm a").format(d);

        viewHolder.date.setText(format);
        viewHolder.fare.setText("$ " + actual.getEstimatedFare().intValue());

        if(uActual.getName() != null)
        {
            viewHolder.user.setText(uActual.getName() + " " + uActual.getLastName());
        }


        viewHolder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction trans = listFragment.getFragmentManager()
                        .beginTransaction();
                /*
                 * IMPORTANT: We use the "root frame" defined in
                 * "root_fragment.xml" as the reference to replace fragment
                 */

                HistoryClientDetail fragment = new HistoryClientDetail();
                Bundle bundle = new Bundle();
                bundle.putSerializable(REQUEST_KEY, actual);
                bundle.putSerializable(USER_KEY, uActual);
                fragment.setArguments(bundle);

                trans.replace(R.id.historyclient_root, fragment);

                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);

                trans.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        View mView;

        public TextView date;
        public TextView fare;
        public TextView user;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            date = (TextView) mView.findViewById(R.id.historyitem_date);
            fare = (TextView) mView.findViewById(R.id.historyitem_fare);
            user = (TextView) mView.findViewById(R.id.historyitem_user);

        }
    }
}
