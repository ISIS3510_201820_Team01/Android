package uniandes.mobile.bipapp.client;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import uniandes.mobile.bipapp.BipFragmentsInterface;
import uniandes.mobile.bipapp.MainActivity;
import uniandes.mobile.bipapp.R;

public class TabsActivityClient extends AppCompatActivity {

    private Toolbar toolbar;
    public ViewPager pager;
    private ViewPagerAdapterClient adapter;
    private TabLayout tabLayout;
    private Button buttonLogOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs_client);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        pager = findViewById(R.id.pager_client);
        adapter = new ViewPagerAdapterClient(getSupportFragmentManager());
        pager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabs_client);
        tabLayout.setupWithViewPager(pager);

        buttonLogOut = (Button) findViewById(R.id.button_logOut);

        pager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(final int position, final float v, final int i2) {
                    }

                    @Override
                    public void onPageSelected(final int position) {

                        if(position == 0)
                        {
                            BipFragmentsInterface fragment = (BipFragmentsInterface) adapter.instantiateItem(pager, position);
                            if (fragment != null) {
                                fragment.fragmentBecameVisible();
                            }

                        }

                    }

                    @Override
                    public void onPageScrollStateChanged(final int position) {
                    }
                });

        buttonLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences currentPickup = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                String docId = currentPickup.getString("pickUpId", "pickUpId");

                SharedPreferences currentDropOff = getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                String docIdDropOff = currentDropOff.getString("dropOffId", "dropOffId");


                SharedPreferences credenciales = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
                credenciales.edit().clear().commit();

                SharedPreferences request = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                request.edit().clear().commit();

                SharedPreferences dropOffValet = getSharedPreferences("DropOffRequestValet", Context.MODE_PRIVATE);
                dropOffValet.edit().clear().commit();

                SharedPreferences dropOffClient = getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
                dropOffClient.edit().clear().commit();

                SharedPreferences currentDropOffPref = getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                currentDropOffPref.edit().clear().commit();

                SharedPreferences parquear = getSharedPreferences("Parquear", Context.MODE_PRIVATE);
                parquear.edit().clear().commit();

                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(TabsActivityClient.this, MainActivity.class);
                startActivity(intent);
                finish();

                return;

               /* if(!docId.equals("pickUpId") || !docIdDropOff.equals("dropOffId"))
                {

                    Toast.makeText(getApplicationContext(),
                            "No es posible cerrar su sesión. Tiene servicios en curso.",
                            Toast.LENGTH_LONG).show();

                }
                else
                {

                    return;
                }*/


            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }
}
