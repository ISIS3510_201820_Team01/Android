package uniandes.mobile.bipapp.client.historyc;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryClientDetail extends Fragment {

    private static final String TAG = "FirebaseLog";

    private FirebaseFirestore database;

    private View mView;


    private TextView fecha;

    private TextView hora;

    private TextView valet;

    private TextView costo;

    private TextView placa;

    private PickUpRequest currentRequest;

    private String docId;

    private PickUpRequest request;

    private User user;



    public HistoryClientDetail() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_history_client_detail, container, false);

        request = (PickUpRequest) getArguments().getSerializable(HistoryListClientAdapter.REQUEST_KEY);
        user = (User) getArguments().getSerializable(HistoryListClientAdapter.USER_KEY);

        fecha = (TextView) mView.findViewById(R.id.historyclient_fecha);
        hora = (TextView) mView.findViewById(R.id.historyclient_hora);
        valet = (TextView) mView.findViewById(R.id.historyclient_nombre);
        costo = (TextView) mView.findViewById(R.id.historyclient_tarifa);
        placa = (TextView) mView.findViewById(R.id.historyclient_placa);

        Date d = new Date(request.getCreationTime().longValue());
        String format = new SimpleDateFormat("MMMM d,  yyyy").format(d);


        fecha.setText(format);


        String format2 = new SimpleDateFormat("h:mm a").format(d);

        hora.setText(format2);

        valet.setText(user.getName());

        costo.setText("$"+request.getEstimatedFare().intValue());

        placa.setText(request.getPlate());

        return mView;
    }


}
