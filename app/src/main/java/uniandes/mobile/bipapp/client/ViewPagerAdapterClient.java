package uniandes.mobile.bipapp.client;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import uniandes.mobile.bipapp.client.currentc.CurrentRootFragment;
import uniandes.mobile.bipapp.TabsFragment;
import uniandes.mobile.bipapp.client.booking.BookingFragment;
import uniandes.mobile.bipapp.client.historyc.HistoryClientRootFragment;

public class ViewPagerAdapterClient extends FragmentPagerAdapter
{
    public ViewPagerAdapterClient(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i)
    {
        Fragment item = null;

        switch(i)
        {
            case 0:
                item = new BookingFragment();
                break;
            case 1:
                item = new CurrentRootFragment();
                break;
            case 2:
                item = new HistoryClientRootFragment();
                break;
        }

        Bundle bundle = new Bundle();
        item.setArguments(bundle);


        return item;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        String title = "";
        switch(position)
        {
            case 0: title="Pedir"; break;
            case 1: title = "Actual"; break;
            case 2: title = "Historial"; break;
        }
        return title;
    }

}
