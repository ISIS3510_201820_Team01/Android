package uniandes.mobile.bipapp.client.currentc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DecimalFormat;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.client.currentc.ReaderActivity;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;


public class ScannerFragment extends Fragment {

    private static final String TAG = "FirebaseLog";

    private FirebaseFirestore database;

    private View mView;
    private Button btnScanner;
    private TextView txtCurrentTime;
    private TextView txtCurrentDistance;
    private TextView txtCurrentFee;
    private TextView txtCurrentValet;

    private PickUpRequest currentRequest;

    private String docIdDropOff;

    private String valetRating;


    public ScannerFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_scanner, container, false);

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        btnScanner = (Button) mView.findViewById(R.id.btnScanner2);
        txtCurrentTime = (TextView) mView.findViewById(R.id.txtCurrentTime);
        txtCurrentDistance = (TextView) mView.findViewById(R.id.txtCurrentDistance);
        txtCurrentFee = (TextView) mView.findViewById(R.id.txtCurrentFee);
        txtCurrentValet = (TextView) mView.findViewById(R.id.txt_valet);

        SharedPreferences currentDropOff = getContext().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
        docIdDropOff = currentDropOff.getString("dropOffId", "dropOffId");

        if(!docIdDropOff.equals("dropOffId"))
        {
            btnScanner.setText("FINALIZAR SERVICIO");
        }


        currentRequest = (PickUpRequest) getArguments().getSerializable(PickupListAdapter.REQUEST_KEY);

        if(currentRequest != null){
            double distancia = calculateDistance(currentRequest.getLatitudeValet(), currentRequest.getLongitudeValet(), currentRequest.getLatitudeClient(), currentRequest.getLongitudeClient());

            txtCurrentDistance.setText(String.format("%.2f", distancia) + " m"+"");

            txtCurrentFee.setText("$ " + currentRequest.getEstimatedFare().intValue());

            txtCurrentTime.setText("7 min");
            //txtCurrentValet.setText("Mario Linares");
            getValetName(currentRequest.getIdValet());

            SharedPreferences preferences = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putString("currentPickUpId", currentRequest.getId());
            //Toast.makeText(getActivity(), "docId Help: " + currentRequest.getId(), Toast.LENGTH_LONG).show();

            editor.putString("currentIdClient", currentRequest.getIdClient());
            editor.putLong("currentConfirmationTime", currentRequest.getConfirmationTime());
            editor.putString("currentIdValet", currentRequest.getIdValet());
            editor.putBoolean("finished", false);

            editor.commit();

        }




        btnScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!docIdDropOff.equals("dropOffId"))
                {
                    SharedPreferences request = getActivity().getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
                    request.edit().clear().commit();

                    SharedPreferences current = getActivity().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                    current.edit().clear().commit();

                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                    tabhost.getTabAt(2).select();

                }
                else
                {
                    Intent intent =  new Intent(getActivity(), ReaderActivity.class);
                    startActivity(intent);

                }
            }
        });

        String textBtnScanner =(String) btnScanner.getText();

        if(textBtnScanner.equals("FINALIZAR SERVICIO")){
            btnScanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    rateValet();
                }
            });
        }


        // Inflate the layout for this fragment
        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences currentRequest = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        boolean finishedActivity = currentRequest.getBoolean("validatedQR", false);

        currentRequest = getActivity().getSharedPreferences("PruebaCurrentPickUp", Context.MODE_PRIVATE);
        String idRequest = currentRequest.getString("docId", "currentPickUpId");



        if(finishedActivity){

            //Toast.makeText(getActivity(), "idRequest Ayuda OnStart1: " + idRequest, Toast.LENGTH_SHORT).show();

            if(!idRequest.equals("currentPickUpId")){

//              Toast.makeText(getActivity(), "idRequest Ayuda OnStart2: " + idRequest, Toast.LENGTH_SHORT).show();
                validateQr(idRequest);

                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                /*
                 * IMPORTANT: We use the "root frame" defined in
                 * "root_fragment.xml" as the reference to replace fragment
                 */

                ConfirmCommentsFragment fragment = new ConfirmCommentsFragment();
                Bundle bundle = new Bundle();
                fragment.setArguments(bundle);

                trans.replace(R.id.current_root, fragment);


                trans.commit();
            }

        }
    }


    public void validateQr(String idRequest){

        if(idRequest.length() > 0){
            database.collection("PickUpServices").document(idRequest)
                    .update("validated", true)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            //Toast.makeText(getActivity(), "Validado QR en Firebase", Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //Toast.makeText(getActivity(), "Error validando QR en Firebase", Toast.LENGTH_LONG).show();

                        }
                    });
        }


    }

    public double calculateDistance(Double latitudeValet, Double longitudeValet, Double latitudeUser, Double longitudeUser)
    {
        double radlatValet = Math.PI * (latitudeValet/180);

        double radlatUser = Math.PI * (latitudeUser/180);

        double radlonValet = Math.PI * (longitudeValet/180);

        double radlonUser = Math.PI * (longitudeUser/180);

        double theta = longitudeValet-longitudeUser;

        double radtheta = Math.PI * (theta/180);

        double dist = Math.sin(latitudeUser) * Math.sin(latitudeValet) + Math.cos(latitudeUser) * Math.cos(latitudeValet) * Math.cos(radtheta);

        dist = Math.acos(dist);

        dist = dist * 180/Math.PI;

        dist = dist * 60 * 1.1515;

        dist = dist * 1.609344;

        return dist;
    }

    public void getValetName(String idValet){

        if(idValet.length() > 0){
            DocumentReference docRef = database.collection("Users").document(idValet);
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        DocumentSnapshot document = task.getResult();
                        String valetName = document.getString("name");
                        String valetLastName = document.getString("lastName");

                        txtCurrentValet.setText(valetName);


                    }
                }
            });
        }
    }


    public void rateValet(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Califica el servicio");

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        input.setKeyListener(DigitsKeyListener.getInstance("012345"));
        input.setSingleLine(true);
        input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});
        input.setText(valetRating);


        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                valetRating = input.getText().toString().trim();

                saveRating();

            }
        });


        builder.show();
    }

    public void saveRating(){
        database.collection("DropOffServices").document(docIdDropOff)
                .update(
                        "valetRating", Integer.parseInt(valetRating)
                );

        Toast.makeText(getActivity(), "Su calificación de " + valetRating + " fue enviada exitosamente.", Toast.LENGTH_LONG).show();
        //Toast.makeText(getActivity(), docIdDropOff, Toast.LENGTH_LONG).show();

        SharedPreferences request = getActivity().getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
        request.edit().clear().commit();

        SharedPreferences current = getActivity().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
        current.edit().clear().commit();

        TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
        tabhost.getTabAt(2).select();

    }


}
