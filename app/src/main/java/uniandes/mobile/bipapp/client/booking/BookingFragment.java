package uniandes.mobile.bipapp.client.booking;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uniandes.mobile.bipapp.BipFragmentsInterface;
import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.client.currentc.WaitingFragment;
import uniandes.mobile.bipapp.valet.currentv.CarObservationActivity;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class BookingFragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, BipFragmentsInterface
{

    private static final String TAG = "MapActivity";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(
            new LatLng(-40, -168), new LatLng(71, 136));

    //widgets
    private AutoCompleteTextView mSearchText;
    private ImageView mGps;
    private View mView;
    private String placa;

    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    private GoogleApiClient mGoogleApiClient;

    //vars
    private Boolean mLocationPermissionsGranted = false;
    private MapView mMapView;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private PlaceInfo mPlace;

    private Button bookButton;
    private Button bookPlaca;

    private FirebaseFirestore database;

    private Boolean isDropOff;


    public BookingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment

        mView = inflater.inflate(R.layout.fragment_booking, container, false);

        placa = "";

        mSearchText = (AutoCompleteTextView) mView.findViewById(R.id.input_search);
        mGps = (ImageView) mView.findViewById(R.id.ic_gps);

        bookButton = (Button) mView.findViewById(R.id.button_book);
        bookPlaca = (Button) mView.findViewById(R.id.btn_cambiarplaca);

        getSavedPreferences();

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
            return null;
        }

        pedirPlaca();

        bookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isDropOff)
                {
                    createDropOffRequest();
                }
                else
                {
                    createPickUpRequest();
                }
            }
        });

        bookPlaca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               pedirPlaca();
            }
        });

  /*      PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this.getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/

        return mView;
    }

    @Override
    public void fragmentBecameVisible() {

        getSavedPreferences();

    }

    private void pedirPlaca( )
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
        builder.setTitle("Placa");
        builder.setMessage("Por favor, ingrese la placa de su vehiculo");

        InputFilter[] fa = new InputFilter[1];

        fa[0] = new InputFilter.LengthFilter(6);

        final EditText input = new EditText(this.getContext());

        input.setFilters(fa);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setText(placa);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                placa = input.getText().toString().trim();
            }
        });

        builder.show();
    }

    private void getSavedPreferences( )
    {
        SharedPreferences currentPickup = getContext().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        String docId = currentPickup.getString("pickUpId", "pickUpId");

        SharedPreferences currentDropOff = getContext().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
        String docIdDropOff = currentDropOff.getString("dropOffId", "dropOffId");


        if(!docId.equals("pickUpId") || !docIdDropOff.equals("dropOffId"))
        {
            if( bookButton != null)
            {
                bookButton.setVisibility(View.GONE);
            }
        }
        else
        {
            bookButton.setVisibility(View.VISIBLE);
        }

        SharedPreferences dropOff = getActivity().getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
        isDropOff = dropOff.getBoolean("DropOff", false);

        if(isDropOff && bookButton != null)
        {
            bookButton.setText("PEDIR MI CARRO DE VUELTA");
        }
        else
        {
            bookButton.setText("PEDIR SERVICIO");
        }
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        getLocationPermission();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }



    public void createPickUpRequest()
    {
        if(mPlace == null)
        {
            Toast.makeText(getContext(),
                    "No ha seleccionado ningún lugar! Esto es necesario para continuar.",
                    Toast.LENGTH_LONG).show();

        }
        else
        {
            if(!verifyConnectivity())
            {
                Toast.makeText(getContext(),
                        "No tiene conexion a internet. No podemos atender sus solicitudes.",
                        Toast.LENGTH_LONG).show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
                builder.setTitle("Confirmar selección");
                builder.setMessage("¿Está seguro de que desea solicitar su servicio en el lugar seleccionado?");

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Long cT = System.currentTimeMillis();
                        Double currentTime = cT.doubleValue();

                        SharedPreferences preferences = getContext().getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
                        String idClient = preferences.getString("idClient", "idClient");

                        PickUpRequest newRequest= new PickUpRequest(false, currentTime, idClient, mPlace.getLatlng().latitude,
                                mPlace.getLatlng().longitude, placa);

                        newRequest.setEstimatedFare(1500.0);
                        newRequest.setConfirmationTime((long)0);
                        newRequest.setIdValet("");
                        newRequest.setLatitudeValet(0.0);
                        newRequest.setLongitudeValet(0.0);
                        newRequest.setQr("");
                        newRequest.setId("");
                        newRequest.setApproved(false);
                        newRequest.setValidated(false);
                        newRequest.setParkLatitude(0.0);
                        newRequest.setParkLongitude(0.0);
                        newRequest.setComments(new ArrayList<String>());
                        newRequest.setCarParked(false);

                        database.collection("PickUpServices")
                                .add(newRequest)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());

                                        SharedPreferences preferences = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();

                                        editor.putString("pickUpId", documentReference.getId());

                                        editor.commit();

                                        TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                                        tabhost.getTabAt(1).select();

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error adding document", e);
                                        Toast.makeText(getContext(),
                                                "No se ha podido resolver su peticion. Vuelva a intentar.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                });

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();


            }
        }
    }

    public void createDropOffRequest()
    {
        if(mPlace == null)
        {
            Toast.makeText(getContext(),
                    "No ha seleccionado ningún lugar! Esto es necesario para continuar.",
                    Toast.LENGTH_LONG).show();

        }
        else
        {
            if(!verifyConnectivity())
            {
                Toast.makeText(getContext(),
                        "No tiene conexion a internet. No podemos atender sus solicitudes......",
                        Toast.LENGTH_LONG).show();
            }
            else
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.getContext());
                builder.setTitle("Confirmar selección");

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Long cT = System.currentTimeMillis();
                        Double currentTime = cT.doubleValue();

                        SharedPreferences preferences = getContext().getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
                        String idClient = preferences.getString("idClient", "idClient");

                        PickUpRequest newRequest= new PickUpRequest(false, currentTime, idClient, mPlace.getLatlng().latitude,
                                mPlace.getLatlng().longitude, "ABC123");

                        newRequest.setEstimatedFare(1500.0);
                        newRequest.setConfirmationTime((long)0);
                        newRequest.setIdValet("");
                        newRequest.setLatitudeValet(0.0);
                        newRequest.setLongitudeValet(0.0);
                        newRequest.setQr("");
                        newRequest.setId("");
                        newRequest.setApproved(false);
                        newRequest.setValidated(false);
                        newRequest.setParkLatitude(0.0);
                        newRequest.setParkLongitude(0.0);
                        newRequest.setComments(new ArrayList<String>());
                        newRequest.setCarParked(false);

                        database.collection("DropOffServices")
                                .add(newRequest)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        Log.d(TAG, "DocumentSnapshot written with ID: " + documentReference.getId());

                                        SharedPreferences preferences = getActivity().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = preferences.edit();

                                        editor.putString("dropOffId", documentReference.getId());

                                        editor.commit();


                                        TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                                        tabhost.getTabAt(1).select();

                                        FragmentTransaction transaction = getFragmentManager()
                                                .beginTransaction();

                                        transaction.replace(R.id.current_root, new WaitingFragment());

                                        transaction.commit();

                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error adding document", e);
                                        Toast.makeText(getContext(),
                                                "No se ha podido resolver su peticion. Vuelva a intentar.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                });

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }

        }
    }

    private void getLocationPermission(){
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionsGranted = true;
                initMap();
            }
            else
                {
                requestPermissions(permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        }else{
            requestPermissions(permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        mLocationPermissionsGranted = false;

        switch(requestCode){
            case LOCATION_PERMISSION_REQUEST_CODE:{
                if(grantResults.length > 0)
                {
                    for(int i = 0; i < grantResults.length; i++){
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED){
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize our map
                    initMap();
                }
            }
        }
    }

    private void initMap(){

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes!",
                    Toast.LENGTH_LONG).show();
        }
        Log.d(TAG, "initMap: initializing map");

        mMapView = (MapView) mView.findViewById(R.id.mapBooking);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);

        }
    }

    private void init(){

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes*",
                    Toast.LENGTH_LONG).show();
        }

        Log.d(TAG, "init: initializing");

        if(mGoogleApiClient == null || !mGoogleApiClient.isConnected()){
            try
            {

                mGoogleApiClient = new GoogleApiClient
                        .Builder(getContext())
                        .addApi(Places.GEO_DATA_API)
                        .addApi(Places.PLACE_DETECTION_API)
                        .enableAutoManage(getActivity(), this)
                        .build();
            }
            catch(Exception e)
            {
                Log.d(TAG, "Problemas con API de Google");
            }
        }



        mSearchText.setOnItemClickListener(mAutocompleteClickListener);

        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(getContext(), mGoogleApiClient,
                LAT_LNG_BOUNDS, null);

        mSearchText.setAdapter(mPlaceAutocompleteAdapter);

        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER){

                    //execute our method for searching
                    geoLocate();
                }

                return false;
            }
        });

        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: clicked gps icon");
                getDeviceLocation();
            }
        });

        hideSoftKeyboard();
    }

    private void geoLocate(){
        Log.d(TAG, "geoLocate: geolocating");

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes!*",
                    Toast.LENGTH_LONG).show();
        }
        else
        {
            String searchString = mSearchText.getText().toString();

            Geocoder geocoder = new Geocoder(getActivity());
            List<Address> list = new ArrayList<>();
            try{
                list = geocoder.getFromLocationName(searchString, 1);
            }catch (IOException e){
                Log.e(TAG, "geoLocate: IOException: " + e.getMessage() );
            }

            if(list.size() > 0){
                Address address = list.get(0);

                Log.d(TAG, "geoLocate: found a location: " + address.toString());
                //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();

                moveCamera(new LatLng(address.getLatitude(), address.getLongitude()), DEFAULT_ZOOM,
                        address.getAddressLine(0));
            }

        }

    }

    private void getDeviceLocation(){
        Log.d(TAG, "getDeviceLocation: getting the devices current location");

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());

        try{
            if(mLocationPermissionsGranted){

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()){
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();

                            if(currentLocation != null)
                            {
                                LatLng userLoc = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                                moveCamera( userLoc,DEFAULT_ZOOM,"Mi ubicación");

                                mPlace = new PlaceInfo();
                                mPlace.setLatlng(userLoc);
                            }
                            else
                            {
                                Toast.makeText(getContext(), "No es posible obtener tu ubicación actual. Verifica tu configuración de GPS y conexión a internet.", Toast.LENGTH_SHORT).show();
                            }

                        }else{
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(getContext(), "No es posible obtener tu ubicación. Verifica permisos y GPS.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage() );
        }
    }

    private void moveCamera(LatLng latLng, float zoom, String title){
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude );
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if(!title.equals("My Location")){
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mMap.addMarker(options);
        }

        hideSoftKeyboard();
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        Log.d(TAG, "onMapReady: map is ready");
        mMap = googleMap;

        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. No podemos atender sus solicitudes..",
                    Toast.LENGTH_LONG).show();
        }
        else
        {
            if (mLocationPermissionsGranted) {
                getDeviceLocation();

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                init();

                if(isDropOff)
                {
                   Toast.makeText(getContext(),
                            "Su carro ha sido estacionado en el parqueadero.",
                            Toast.LENGTH_LONG).show();

                }
            }
        }

    }

    private void hideSoftKeyboard(){
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        Toast.makeText(getContext(), "Verifica tu conexión a internet. No podemos mostrar el mapa correctamente.", Toast.LENGTH_SHORT).show();
    }

   /* public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this.getActivity(), data);

                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, place.getId());
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            }
        }
    }*/

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }


    private AdapterView.OnItemClickListener mAutocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            hideSoftKeyboard();

            if(!verifyConnectivity())
            {
                Toast.makeText(getContext(),
                        "No tiene conexion a internet. No podemos atender sus solicitudes...",
                        Toast.LENGTH_LONG).show();
            }
            else{

                final AutocompletePrediction item = mPlaceAutocompleteAdapter.getItem(i);
                final String placeId = item.getPlaceId();

                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                        .getPlaceById(mGoogleApiClient, placeId);
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
            }

        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {

            if(!verifyConnectivity())
            {
                Toast.makeText(getContext(),
                        "No tiene conexion a internet. No podemos atender sus solicitudes....",
                        Toast.LENGTH_LONG).show();
            }
            else
            {
                if(!places.getStatus().isSuccess()){
                    Log.d(TAG, "onResult: Place query did not complete successfully: " + places.getStatus().toString());
                    places.release();
                    return;
                }
                final Place place = places.get(0);

                try{
                    mPlace = new PlaceInfo();
                    mPlace.setName(place.getName().toString());
                    Log.d(TAG, "onResult: name: " + place.getName());
                    mPlace.setAddress(place.getAddress().toString());
                    Log.d(TAG, "onResult: address: " + place.getAddress());
//                mPlace.setAttributions(place.getAttributions().toString());
//                Log.d(TAG, "onResult: attributions: " + place.getAttributions());
                    mPlace.setId(place.getId());
                    Log.d(TAG, "onResult: id:" + place.getId());
                    mPlace.setLatlng(place.getLatLng());
                 /*   Log.d(TAG, "onResult: latlng: " + place.getLatLng());
                    mPlace.setRating(place.getRating());
                    Log.d(TAG, "onResult: rating: " + place.getRating());
                    mPlace.setPhoneNumber(place.getPhoneNumber().toString());
                    Log.d(TAG, "onResult: phone number: " + place.getPhoneNumber());
                    mPlace.setWebsiteUri(place.getWebsiteUri());
                    Log.d(TAG, "onResult: website uri: " + place.getWebsiteUri());

                    Log.d(TAG, "onResult: place: " + mPlace.toString());*/
                }catch (NullPointerException e){
                    Log.e(TAG, "onResult: NullPointerException: " + e.getMessage() );
                }

                moveCamera(new LatLng(place.getViewport().getCenter().latitude,
                        place.getViewport().getCenter().longitude), DEFAULT_ZOOM, mPlace.getName());

                places.release();

            }

        }
    };


}
