package uniandes.mobile.bipapp.client.currentc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.currentv.CarObservationActivity;

public class ReaderActivity extends AppCompatActivity {

    private Button btnScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);



        btnScanner = (Button) findViewById(R.id.btnScanner);

        final Activity activity = this;

        IntentIntegrator integrator = new IntentIntegrator(activity);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(result != null){
            if(result.getContents() == null){
                Toast.makeText(this, "Cancelaste la validación", Toast.LENGTH_SHORT).show();
                finish();
            } else{
                //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();

                SharedPreferences preferences = getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
                String idClient = preferences.getString("idClient", "idValet");

                SharedPreferences currentRequest = getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                String idValet = currentRequest.getString("currentIdValet", "");
                Long confirmationTime = currentRequest.getLong("currentConfirmationTime", 0L);

                String confirmationTimeString = Long.toString(confirmationTime);

                String resultQR = result.getContents();
                //Toast.makeText(this, "Resultado: " + resultQR, Toast.LENGTH_LONG).show();
                String correctQR = idClient + idValet;
                //Toast.makeText(this, "Correcto: " + correctQR, Toast.LENGTH_LONG).show();
                if(resultQR.equals(correctQR)){
                    Toast.makeText(this, "Sí es tu Valet!", Toast.LENGTH_LONG).show();

                    SharedPreferences.Editor editor = currentRequest.edit();
                    editor.putBoolean("validatedQR", true);

                    editor.commit();


                } else{
                    Toast.makeText(this, "No es tu Valet!", Toast.LENGTH_LONG).show();

                }

                //currentRequest.edit().clear().commit();

                finish();
            }


        } else{
            super.onActivityResult(requestCode, resultCode, data);
        }





    }
}
