package uniandes.mobile.bipapp.client.historyc;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.User;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryListClientFragment extends Fragment {

    private static final String TAG = "FirebaseLog";
    private static final int REQUEST_CODE = 1;

    private RecyclerView pickuplist;

    private FirebaseFirestore database;

    private List<PickUpRequest> requestList;

    private List<User> usersList;

    private HistoryListClientAdapter adapter;

    private String idClient;



    public HistoryListClientFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_history_list_client, container, false);

        requestList = new ArrayList<>();
        usersList = new ArrayList<>();
        adapter = new HistoryListClientAdapter(requestList, usersList,this);

        pickuplist = (RecyclerView) view.findViewById(R.id.HistoryClientList);
        pickuplist.setHasFixedSize(true);

        pickuplist.setAdapter(adapter);
        pickuplist.setLayoutManager(new LinearLayoutManager(getActivity()));


        SharedPreferences preferences = getContext().getSharedPreferences("Credenciales", Context.MODE_PRIVATE);
        idClient = preferences.getString("idClient", "idClient");

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(!verifyConnectivity())
        {
            Toast.makeText(getContext(),
                    "No tiene conexion a internet. La lista mostrada no se actualizará en tiempo real.",
                    Toast.LENGTH_LONG).show();
        }

        getPickUpList();

    }

    public void getPickUpList( )
    {
        database.collection("PickUpServices").addSnapshotListener(
                new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d(TAG, "Error getting documents: " + e.getMessage());
                            return;
                        }

                        try
                        {
                            for (DocumentChange dc : queryDocumentSnapshots.getDocumentChanges()) {
                                switch (dc.getType()) {
                                    case ADDED:

                                        Log.d(TAG, "New valet request: " + dc.getDocument().getData());
                                        PickUpRequest req = dc.getDocument().toObject(PickUpRequest.class);

                                        if(req.getIdClient().equals(idClient))
                                        {
                                            req.setId(dc.getDocument().getId());

                                            User u = new User( );
                                            u.setId(req.getIdValet());
                                            getUserFromDB(u);

                                            requestList.add(req);
                                            usersList.add(u);

                                            Log.d(TAG, "Req id:" + req.getId());

                                            adapter.notifyDataSetChanged();
                                        }

                                        break;
                                    case MODIFIED:

                                        break;
                                    case REMOVED:
                                        Log.d(TAG, "Removed request: " + dc.getDocument().getData());
                                        PickUpRequest removedReq = dc.getDocument().toObject(PickUpRequest.class);
                                        removeRequestFromList(removedReq);
                                        break;
                                }
                            }


                        }
                        catch(Exception exp)
                        {
                            Log.d(TAG, exp.getMessage());

                        }


                    }
                });
    }

    private void getUserFromDB(final User u)
    {
        DocumentReference docRef = database.collection("Users").document(u.getId());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        u.setEmail(document.getString("email"));
                        u.setName(document.getString("name"));
                        u.setLastName(document.getString("lastName"));
                        u.setPhone(document.getString("phone"));

                        adapter.notifyDataSetChanged();

                    } else {
                        Log.d(TAG, "No such client");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

    }

    public void removeRequestFromList(PickUpRequest request)
    {
        String id = request.getId();
        int size = requestList.size();
        for(int i = 0; i < size; i++)
        {
            String idActual = requestList.get(i).getId();

            if(id.equals(idActual))
            {
                requestList.remove(i);
                usersList.remove(i);
            }
        }

        adapter.notifyDataSetChanged();

    }

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }

}
