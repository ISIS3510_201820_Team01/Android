package uniandes.mobile.bipapp.client.historyc;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uniandes.mobile.bipapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryClientRootFragment extends Fragment {


    public HistoryClientRootFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_history_client_root, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        /*
         * When this container fragment is created, we fill it with our first
         * "real" fragment
         */
        transaction.replace(R.id.historyclient_root, new HistoryListClientFragment());

        transaction.commit();

        return view;

    }

}
