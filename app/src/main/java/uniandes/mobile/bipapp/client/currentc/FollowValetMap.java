package uniandes.mobile.bipapp.client.currentc;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.ListenerRegistration;

import java.util.List;

import pl.droidsonroids.gif.GifImageView;
import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.valet.currentv.GeneratorActivity;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;


public class FollowValetMap extends Fragment implements OnMapReadyCallback{

    private static final String TAG = "MapsFragment";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final float DEFAULT_ZOOM = 15f;
    private static final int REQUEST_CODE = 1;
    private ListenerRegistration snapShot;

    private String docId;
    private String docIdDropOff;


    private GoogleMap mMap;
    private MapView mMapView;
    private View mView;
    private Location mLocation;
    private LocationRequest mLocationRequest;

    private Marker pickUpMarker;

    private FusedLocationProviderClient mFusedLocationProviderClient;

    private PickUpRequest currentRequest;

    private Button btnDropOff;

    private FirebaseFirestore database;

    private Marker mDriverMarker;

    public FollowValetMap() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_follow_valet_map, container, false);

        SharedPreferences currentPickup = getContext().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        docId = currentPickup.getString("pickUpId", "pickUpId");

        SharedPreferences currentDropOff = getContext().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
        docIdDropOff = currentPickup.getString("dropOffId", "dropOffId");

        String collection = "";
        String idDocument = "";

        if(!docId.equals("pickUpId"))
        {
            collection = "PickUpServices";
            idDocument = docId;
        }
        else if(!docIdDropOff.equals("dropOffId"))
        {
            collection = "DropOffServices";
            idDocument = docIdDropOff;
        }


        //currentPickup.edit().clear().commit();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());


        try
        {
            database = FirebaseFirestore.getInstance();

            DocumentReference docRef = database.collection(collection).document(idDocument);
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            currentRequest = document.toObject(PickUpRequest.class);
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        btnDropOff = (Button) mView.findViewById(R.id.btnDropOff);
        btnDropOff.setVisibility(View.GONE);

        btnDropOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(verifyConnectivity())
                {
                    SharedPreferences preferences = getActivity().getSharedPreferences("DropOffRequest", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();

                    editor.putBoolean("DropOff", true);

                    editor.commit();

                    TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                    tabhost.getTabAt(0).select();
                }
                else{
                    Toast.makeText(getActivity(), "Sin internet, no se puede realizar la acción", Toast.LENGTH_LONG).show();
                }
            }
        });

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMapView = (MapView) mView.findViewById(R.id.map);
        if (mMapView != null) {
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);

        }

    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    getDeviceLocation();


                } else {
                    // permission denied, boo!

                    Toast.makeText(getContext(), "No se pudo acceder a su localizacion. Falta de permisos de su parte.", Toast.LENGTH_LONG).show();

                }
                return;
            }
        }
    }
*/



    //Gets the device location
    private void getDeviceLocation() {


        try {

            Task location = mFusedLocationProviderClient.getLastLocation();
            location.addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: found location!");
                        Location currentLocation = (Location) task.getResult();


                        moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);

                    } else {
                        Log.d(TAG, "onComplete: current location is null");
                        Toast.makeText(getActivity(), "Unable to get current location", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: Exception: " + e.getMessage());
        }
    }

    //Adjust camera in the map
    private void moveCamera(LatLng latLng, float zoom) {

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    public boolean verifyConnectivity( )
    {
        ConnectivityManager cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean todoOk = false;
        if (isConnected)
        {
            boolean isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

            boolean isDatos1 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            boolean isDatos2 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_SUPL;
            boolean isDatos3 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_DUN;
            boolean isDatos4 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_HIPRI;
            boolean isDatos5 = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE_MMS;

            boolean isDatos = isDatos1 || isDatos2 || isDatos3 || isDatos4 || isDatos5;

            todoOk = isDatos || isWiFi;
        }


        return todoOk;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                mMap.setMyLocationEnabled(true);
                getDeviceLocation();
            }else{
                checkLocationPermission();
            }
        }

        snapShot = database.collection("PickUpServices").document(docId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable DocumentSnapshot documentSnapshot, @javax.annotation.Nullable FirebaseFirestoreException e) {
                Log.d("FOLLOW VALET MAP", "Cambio algo");


                if(e != null){
                    Toast.makeText(getActivity(), "Error Cargando ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.toString());
                    return;
                }

                if(documentSnapshot != null && documentSnapshot.exists()){

                    PickUpRequest req = documentSnapshot.toObject(PickUpRequest.class);

                    if(req.getCarParked()) {

                        btnDropOff.setVisibility(View.VISIBLE);
                    }

                    LatLng driverLatLng = new LatLng(req.getLatitudeValet(),req.getLongitudeValet());
                    if(mDriverMarker != null){
                        mDriverMarker.remove();
                    }

                    mDriverMarker = mMap.addMarker(new MarkerOptions().position(driverLatLng).title("Su valet"));
                    moveCamera(new LatLng(driverLatLng.latitude, driverLatLng.longitude), DEFAULT_ZOOM);

                }
            }
        });
    }

    private void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Solicitud de permisos")
                        .setMessage("Por favor, proporcione los permisos necesarios")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            }
            else{
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case 1:{
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        //mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        getDeviceLocation();
                        mMap.setMyLocationEnabled(true);
                    }
                } else{
                    Toast.makeText(getContext(), "Por favor, proporcione los permisos necesarios", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }


  /*  @Override
    public void onMapReady(GoogleMap googleMap) {

        MapsInitializer.initialize(getContext());
        mMap = googleMap;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000*5);
        mLocationRequest.setFastestInterval(1000*3);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        int permissionCheck = ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // ask permissions
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }
        else
        {
            getDeviceLocation();
        }



        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

    }*/


}
