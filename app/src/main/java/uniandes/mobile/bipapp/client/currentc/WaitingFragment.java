package uniandes.mobile.bipapp.client.currentc;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;

import javax.annotation.Nullable;

import pl.droidsonroids.gif.GifImageView;
import uniandes.mobile.bipapp.PickUpRequest;
import uniandes.mobile.bipapp.R;
import uniandes.mobile.bipapp.client.TabsActivityClient;
import uniandes.mobile.bipapp.client.currentc.ScannerFragment;
import uniandes.mobile.bipapp.valet.pickup.PickupListAdapter;


public class WaitingFragment extends Fragment {

    private static final String TAG = "FirebaseLog";

    private FirebaseFirestore database;

    private DocumentReference docRef;

    private TextView text;

    private GifImageView gif;

    private Button btnCancel;

    private String docId;

    private String docIdDropOff;

    private ListenerRegistration snapShot;

    public WaitingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_waiting, container, false);

        Log.d("WAITING", "Entro al onCreateView");


        btnCancel =(Button) view.findViewById(R.id.button_cancel);

        try
        {
            database = FirebaseFirestore.getInstance();
        }
        catch(Exception exc)
        {
            Log.d(TAG, exc.getMessage());
        }

        SharedPreferences currentPickup = getContext().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
        docId = currentPickup.getString("pickUpId", "pickUpId");

        SharedPreferences currentDropOff = getContext().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
        docIdDropOff = currentDropOff.getString("dropOffId", "dropOffId");


        SharedPreferences pruebaCurrent = getContext().getSharedPreferences("PruebaCurrentPickUp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pruebaCurrent.edit();

        editor.putString("docId", docId);
        editor.commit();

        //currentPickup.edit().clear().commit();

        if(!docId.equals("pickUpId"))
        {
            getCurrentPickUpService();

        }
        else if(!docIdDropOff.equals("dropOffId"))
        {
            getCurrentDropOffService( );
        }
        else
        {
            text = (TextView) view.findViewById(R.id.text_waiting);
            gif = (GifImageView) view.findViewById(R.id.gif_image);

            text.setText("No tienes ningún servicio de Valet en curso.");
            gif.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
        }


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!docIdDropOff.equals("dropOffId"))
                {
                    cancelRequestDropOff();
                }
                else
                {
                    cancelRequestPickUp();

                }

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onStop( ) {

        super.onStop();

        if(snapShot != null)
        {
            snapShot.remove();
        }

    }

    public void getCurrentDropOffService(){

        Log.d("WAITING FRAG", docIdDropOff);

        snapShot = database.collection("DropOffServices").document(docIdDropOff).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                Log.d("WAITING FRAG", "Cambio algo ");


                if(e != null){
                    Toast.makeText(getActivity(), "Error Cargando ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.toString());
                    return;
                }

                if(documentSnapshot != null && documentSnapshot.exists()){

                    PickUpRequest req = documentSnapshot.toObject(PickUpRequest.class);

                    if(req.getConfirmed()) {

                        FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        /*
                         * IMPORTANT: We use the "root frame" defined in
                         * "root_fragment.xml" as the reference to replace fragment
                         */

                        ScannerFragment fragment = new ScannerFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(PickupListAdapter.REQUEST_KEY,req);
                        fragment.setArguments(bundle);

                        trans.replace(R.id.current_root, fragment);


                        trans.commit();

                        //Bundle bundle = new Bundle();
                        //bundle.putSerializable(PickupListAdapter.REQUEST_KEY, req);
                        //ScannerFragment fragment = new ScannerFragment();
                        //fragment.setArguments(bundle);

                        //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.waitingFragment, fragment).commit();
                    }


                }
            }
        });

    }

    public void getCurrentPickUpService(){

        Log.d("WAITING FRAG", docId);

        snapShot = database.collection("PickUpServices").document(docId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                Log.d("WAITING FRAG", "Cambio algo ");


                if(e != null){
                    Toast.makeText(getActivity(), "Error Cargando ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.toString());
                    return;
                }

                if(documentSnapshot != null && documentSnapshot.exists()){

                    PickUpRequest req = documentSnapshot.toObject(PickUpRequest.class);

                    if(req.getConfirmed()) {

                        FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        /*
                         * IMPORTANT: We use the "root frame" defined in
                         * "root_fragment.xml" as the reference to replace fragment
                         */

                        ScannerFragment fragment = new ScannerFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(PickupListAdapter.REQUEST_KEY,req);
                        fragment.setArguments(bundle);

                        trans.replace(R.id.current_root, fragment);


                        trans.commit();

                        //Bundle bundle = new Bundle();
                        //bundle.putSerializable(PickupListAdapter.REQUEST_KEY, req);
                        //ScannerFragment fragment = new ScannerFragment();
                        //fragment.setArguments(bundle);

                        //getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.waitingFragment, fragment).commit();
                    }


                }
            }
        });

    }

    public void cancelRequestPickUp(){
        if(database != null){
            database.collection("PickUpServices").document(docId)
                    .delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getActivity(), "Solicitud cancelada exitosamente.", Toast.LENGTH_LONG).show();
                            if(snapShot != null)
                            {
                                snapShot.remove();
                            }
                            SharedPreferences request = getActivity().getSharedPreferences("CurrentPickUp", Context.MODE_PRIVATE);
                            request.edit().clear().commit();
                            /*TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                            tabhost.getTabAt(0).select();*/
                            ((TabsActivityClient)getActivity()).pager.setCurrentItem(0, true);

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Error cancelando la solicitud.", Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }

    public void cancelRequestDropOff(){
        if(database != null){
            database.collection("DropOffServices").document(docIdDropOff)
                    .delete()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(getActivity(), "Solicitud cancelada exitosamente.", Toast.LENGTH_LONG).show();
                            if(snapShot != null)
                            {
                                snapShot.remove();
                            }
                            SharedPreferences currentDropOff = getContext().getSharedPreferences("CurrentDropOff", Context.MODE_PRIVATE);
                            currentDropOff.edit().clear().commit();
                            TabLayout tabhost = (TabLayout) getActivity().findViewById(R.id.tabs_client);
                            tabhost.getTabAt(0).select();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(), "Error cancelando la solicitud.", Toast.LENGTH_LONG).show();
                        }
                    });
        }
    }


}
